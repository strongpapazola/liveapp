from flask import Flask, request, jsonify

app = Flask(__name__)

@app.route("/barang", methods=['GET']) # Fitur Ambil Semua Data
def getbarang():
    try:
        return jsonify({"data":[{"nama":"buku", "harga":"5000"},{"nama":"pensil", "harga":"4000"}], "code": 200}), 200
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/barang/<int:id>", methods=['GET']) # Fitur Ambil Salah 1 Data
def getbarangbyid(id):
    try:
        return jsonify({"data":{"nama":"buku", "harga":"5000"}, "code": 200}), 200
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

if "__main__" == __name__:
    # app.run(debug=True, host="0.0.0.0", port=5000)
    app.run(debug=True, port=1000)