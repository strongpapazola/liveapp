import requests
from flask import Flask, request, jsonify

app = Flask(__name__)

@app.route("/getbarang", methods=['GET'])
def getbarang():
    try:
        return requests.get('http://localhost:7010/getbarang').json()
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/getgempa", methods=['GET'])
def getgempa():
    try:
        return requests.get('http://localhost:7001/getgempa').json()
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

if "__main__" == __name__:
    app.run(debug=True, host="0.0.0.0", port=7000)
#    app.run(debug=True, port=1000)
