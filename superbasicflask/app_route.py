import requests
from flask import Flask, request, jsonify

app = Flask(__name__)

@app.route("/getbarang", methods=['GET'])
def getbarang():
    try:
        result = [
            {
                "nama" : "buku",
                "jumlah": 3
            },
            {
                "nama" : "pensil",
                "jumlah": 2
            },
        ]
        return jsonify({"data":result, "code": 200}), 200
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/getgempa", methods=['GET'])
def getgempa():
    try:
        result = requests.get('https://data.bmkg.go.id/DataMKG/TEWS/autogempa.json').json()
        return jsonify({"data" : result, 'code' : 200}), 200
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

if "__main__" == __name__:
    # app.run(debug=True, host="0.0.0.0", port=5000)
    app.run(debug=True, port=1000)