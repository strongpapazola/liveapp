from logging import debug
from flask import Flask, request, jsonify
import mysql.connector
from functools import wraps

app = Flask(__name__)

mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    passwd="",
    database="liveapp"
)        

@app.route("/barang/<int:id>", methods=['PUT'])
def updatebarangbyid(id):
    try:
        data = request.json
        cursor = mydb.cursor()
        sql = "UPDATE barang SET nama = %s, harga = %s WHERE id = %s"
        value = (data['nama'], data['harga'], id)
        cursor.execute(sql, value)
        mydb.commit()
        return jsonify({"data":"1 Record Affected!", "code": 200}), 200
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/barang/<int:id>", methods=['DELETE'])
def deletebarangbyid(id):
    try:
        cursor = mydb.cursor()
        cursor.execute("DELETE FROM barang WHERE id = %s" % (id,))
        return jsonify({"data":"1 Record Deleted!", "code": 200}), 200
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

if "__main__" == __name__:
    # app.run(debug=True, host="0.0.0.0", port=5000)
    app.run(debug=True, port=2000)