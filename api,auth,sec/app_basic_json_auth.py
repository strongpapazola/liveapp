from logging import debug
from flask import Flask, json, request, jsonify
import mysql.connector
import jwt
import datetime
from functools import wraps

app = Flask(__name__)
app.config["SECRET_KEY"] = "@(*)#$iowpmcieupONPOI09834"

def token_required(f):
    @wraps(f)
    def decorator(*args, **kwargs):
        token = request.headers.get("JWT")
        if not token:
            return jsonify({"data":"Token Required", "code": 403}), 403        
        try:
            data = jwt.decode(token, app.config["SECRET_KEY"])
            # print(data)
            # return data
        except:
            return jsonify({"data":"Token Invalid", "code": 403}), 403
        return f(*args, **kwargs)
    return decorator

@app.route("/login", methods=["POST"])
def login():
    try:
        data = request.json
        if (data['username'] == "admin" and data['password'] == "admin123"):
            token = jwt.encode({"data": data['username']+"-"+request.remote_addr, "exp": datetime.datetime.utcnow()+datetime.timedelta(minutes=30)}, app.config["SECRET_KEY"])
            return jsonify({"data": token.decode("UTF-8") , "code": 200}), 200
        else:
            return jsonify({"data":"Login Was Invalid", "code": 403}), 403
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/barang", methods=['GET'])
@token_required
def getbarang():
    try:
        result = [
            {
                "nama" : "brangkas",
                "harga" : 100000
            },
            {
                "nama" : "buku",
                "harga" : 5000
            },
        ]
        return jsonify({"data":result, "code": 200}), 200
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

if "__main__" == __name__:
    # app.run(debug=True, host="0.0.0.0", port=5000)
    app.run(debug=True, port=1000)